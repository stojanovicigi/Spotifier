//
//  AlbumGridCell.swift
//  Spotifier
//
//  Created by Nemanja Gagic on 6/28/17.
//  Copyright © 2017 Nemanja Gagic. All rights reserved.
//

import Foundation
import Kingfisher

final class AlbumGridCell: UICollectionViewCell, NibReusableView {
    
    @IBOutlet weak var photoView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var artistLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        cleanup()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        cleanup()
    }
    
    fileprivate func cleanup() {
        photoView.image = nil
        nameLabel.text = nil
        artistLabel.text = nil
    }
    
    func configured(with album: Album) -> AlbumGridCell {
        self.nameLabel.text = album.name
        self.artistLabel.text = album.artistNames
        if let url = album.imageURL {
            self.photoView.kf.setImage(with: url)
        } else {
            self.photoView.image = UIImage(named: "EmptyImage")
        }
        return self
    }
}























