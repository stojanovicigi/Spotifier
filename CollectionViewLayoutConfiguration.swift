//
//  CollectionViewLayoutConfiguration.swift
//  Spotifier
//
//  Created by Nemanja Gagic on 7/5/17.
//  Copyright © 2017 Nemanja Gagic. All rights reserved.
//

import UIKit

extension UICollectionView {
    
    func sizeForItemForHorizontalScrolling(_ indexPath: IndexPath, columns: CGFloat, layout: UICollectionViewLayout) -> CGSize {
        
        let bounds = self.bounds
        let flowLayout = layout as! UICollectionViewFlowLayout
        var size = flowLayout.itemSize
        let availableHeight = bounds.height - (columns-1) * flowLayout.minimumInteritemSpacing
        
        size.height = availableHeight / columns
        size.width = size.height
        return size
    }
    
    func sizeForItemForVerticalScrolling(_ indexPath: IndexPath, rows: CGFloat, layout: UICollectionViewLayout) -> CGSize {
        
        let bounds = self.bounds
        let flowLayout = layout as! UICollectionViewFlowLayout
        var size = flowLayout.itemSize
        let availableWidth = bounds.width - (rows-1) * flowLayout.minimumInteritemSpacing
        
        size.width = availableWidth / rows
        size.height = size.width
        return size
    }
    
    func sizeForItemFromUI(_ collectionView: UICollectionView,
                           layout: UICollectionViewLayout,
                           sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let bounds = collectionView.bounds
        let flowLayout = layout as! UICollectionViewFlowLayout
        var size = flowLayout.itemSize
        size.width = bounds.width
        return size
    }
}

    
    
    
    



