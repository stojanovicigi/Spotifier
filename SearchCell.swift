//
//  SearchCell.swift
//  Spotifier
//
//  Created by Nemanja Gagic on 6/22/17.
//  Copyright © 2017 Nemanja Gagic. All rights reserved.
//

import UIKit
import Kingfisher

class SearchCell: UICollectionViewCell, NibReusableView {
    
    @IBOutlet fileprivate weak var photoView: UIImageView!
    @IBOutlet fileprivate weak var nameLabel: UILabel!
    @IBOutlet fileprivate weak var popularityLabel: UILabel!
    @IBOutlet fileprivate weak var followersLabel: UILabel!
    @IBOutlet fileprivate weak var albumName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        cleanup()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        cleanup()
    }
    
    fileprivate func cleanup() {
        photoView.image = nil
        nameLabel.text = nil
        popularityLabel.text = nil
        followersLabel.text = nil
        albumName.text = nil
    }
    
    func configuredWithArtist(with artist: Artist) -> SearchCell {
        self.nameLabel.text = artist.name
        
        if let url = artist.imageURL {
            self.photoView.kf.setImage(with: url)
        } else {
            self.photoView.image = UIImage(named: "EmptyImage")
        }
        
        if artist.popularity > 0 {
            self.popularityLabel.text = "Popularity: \(artist.popularity)"
        }
        
        if artist.followersCount > 0 {
            self.followersLabel.text = "\(artist.followersCount) followers"
        }
        return self
    }
    
    func configuredWithAlbum(with album: Album) -> SearchCell {
        nameLabel.text = album.name
        
        if let url = album.imageURL {
            self.photoView.kf.setImage(with: url)
        } else {
            self.photoView.image = UIImage(named: "EmptyImage")
        }
        
        if let albumName = album.artistNames {
            self.popularityLabel.text = albumName
        } else {
            self.popularityLabel.text = "Unknown artist"
        }
        
        return self
    }
    
    func configuredWithTrack(with track: Track) -> SearchCell {
        self.nameLabel.text = track.name
        self.albumName.text = track.artistNames
        self.photoView.image = UIImage(named: "play song 2")
        return self
    }
}


