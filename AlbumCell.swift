//
//  AlbumCell.swift
//  Spotifier
//
//  Created by Nemanja Gagic on 7/4/17.
//  Copyright © 2017 Nemanja Gagic. All rights reserved.
//

import UIKit
import Kingfisher

final class AlbumCell: UICollectionViewCell, NibReusableView {
    
    
    @IBOutlet weak var photoView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        cleanup()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        cleanup()
    }
    
    func cleanup() {
        nameLabel.text = nil
        photoView.image = nil
    }
    
    func configuredCell(_ cell: AlbumCell, with album: Album) -> AlbumCell {
        self.nameLabel.text = album.name
        if let url = album.imageURL {
            self.photoView.kf.setImage(with: url)
        } else {
            self.photoView.image = UIImage(named: "EmptyImage")
        }
        return self
    }
    
    
    func configured(with album: Album) -> AlbumCell {
        self.nameLabel.text = album.name
        if let url = album.imageURL {
            self.photoView.kf.setImage(with: url)
        } else {
            self.photoView.image = UIImage(named: "EmptyImage")
        }
        return self
    }
}

