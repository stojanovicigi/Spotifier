//
//  ArtistGreedController.swift
//  Spotifier
//
//  Created by Nemanja Gagic on 6/12/17.
//  Copyright © 2017 Nemanja Gagic. All rights reserved.
//

import UIKit
import CoreData

class MainVC: UIViewController, StoryboardLoadable, NeedsDependency {
    
    var dependency: Dependency? {
        didSet {
            if let vc = searchController {
                vc.dependency = dependency
            }
            if !self.isViewLoaded { return }
            prepareDataSource()
        }
    }
    
    var dataManager: DataManager? { return dependency?.dataManager }
    var moc: NSManagedObjectContext? { return dependency?.coreDataContext }
        
    var frcArtist: NSFetchedResultsController<Artist>?
    var frcAlbum: NSFetchedResultsController<Album>?
    var frcTrack: NSFetchedResultsController<Track>?
    
    fileprivate var searchController: SearchController?
    
    @IBOutlet fileprivate weak var collectionViewArtist: UICollectionView!
    @IBOutlet fileprivate weak var collectionViewAlbum: UICollectionView!
    @IBOutlet fileprivate weak var collectionViewTrack: UICollectionView!
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK:- ViewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // reload labels, buttons, views etc
        
        prepareDataSource()
        setupNotificationHandlers()
        
//        if artists.count == 0 {
//            let vc = SearchController.initial()
//            show(vc, sender: self)
//        }
        
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
}


fileprivate extension MainVC {
    func setupNotificationHandlers() {
        NotificationCenter.default.addObserver(
            forName: DataManager.Notify.dataPreloadCompleted.name,
            object: nil,
            queue: OperationQueue.main) {
                [weak self] notification in
                guard let `self` = self else { return }
                
                // Process notification
                // notification.object      // Any?
                // notification.userInfo    // [AnyHashable: Any]?
                
                self.prepareDataSource()
        }
    }
}


// MARK:- prepareDataSource()
extension MainVC {
    
    func prepareDataSource() {
        prepareArtistDataSource()
        prepareAlbumDataSource()
        prepareTrackDataSource()
    }
}


extension MainVC {
    
    fileprivate func prepareArtistDataSource() {
        guard let moc = moc else { return }
        
        // MARK: Fetch Request
        let fr: NSFetchRequest<Artist> = Artist.fetchRequest()
        
        // MARK: Sort Descriptors
        let sort = NSSortDescriptor(key: Artist.Attributes.popularity, ascending: false)
        let sort2 = NSSortDescriptor(key: Artist.Attributes.followersCount, ascending: false)
        fr.sortDescriptors = [sort, sort2]
        
        // MARK: Fetched Results Controller
        frcArtist = NSFetchedResultsController(fetchRequest: fr,
                                               managedObjectContext: moc,
                                               sectionNameKeyPath: nil,
                                               cacheName: nil)        
        
        frcArtist?.delegate = self
        
        if ((try? frcArtist?.performFetch()) != nil) {
            collectionViewArtist.reloadData()
            // if no data, jump to search
            //            if frcArtist?.fetchedObjects?.count ?? 0 == 0 {
            //                openSearch()
            //            }
        }
    }
    
    fileprivate func prepareAlbumDataSource() {
        guard let moc = moc else { return }
        
        let fr: NSFetchRequest<Album> = Album.fetchRequest()
        
        let sort = NSSortDescriptor(key: Album.Attributes.dateReleased, ascending: false)
        let sort2 = NSSortDescriptor(key: Album.Attributes.albumId, ascending: false)
        fr.sortDescriptors = [sort, sort2]
        
        frcAlbum = NSFetchedResultsController(fetchRequest: fr,
                                              managedObjectContext: moc,
                                              sectionNameKeyPath: nil,
                                              cacheName: nil)
        
        frcAlbum?.delegate = self
        
        if ((try? frcAlbum?.performFetch()) != nil) {
            collectionViewAlbum.reloadData()
        }
    }
    
    fileprivate func prepareTrackDataSource() {
        guard let moc = moc else { return }
        
        let fr: NSFetchRequest<Track> = Track.fetchRequest()
        
        let sort = NSSortDescriptor(key: Track.Attributes.popularity, ascending: false)
        fr.sortDescriptors = [sort]
        
        frcTrack = NSFetchedResultsController(fetchRequest: fr,
                                              managedObjectContext: moc,
                                              sectionNameKeyPath: nil,
                                              cacheName: nil)
        
        frcTrack?.delegate = self
        
        if ((try? frcTrack?.performFetch()) != nil) {
            collectionViewTrack.reloadData()
        }
    }
}


// MARK:- FRC Delegate
extension MainVC: NSFetchedResultsControllerDelegate {
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        //    reload labels, buttons, views etc
        
        if controller == frcArtist {
            collectionViewArtist.reloadData()
        } else if controller == frcAlbum {
            collectionViewAlbum.reloadData()
        } else {
            collectionViewTrack.reloadData()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        switch collectionView {
        case collectionViewArtist:
            openArtist(index: indexPath.row)
        case collectionViewAlbum:
            openAlbum(index: indexPath.row)
        default:
            openTrack(index: indexPath.row)
        }
    }
}

// MARK:- UICollectionView DataSource
extension MainVC: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        switch collectionView {
        case collectionViewArtist:
            return fetchedNumberOfSections(in: collectionView, with: frcArtist)
        case collectionViewAlbum:
            return fetchedNumberOfSections(in: collectionView, with: frcAlbum)
        default:
            return fetchedNumberOfSections(in: collectionView, with: frcTrack)
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        switch collectionView {
        case collectionViewArtist:
            return fetchedItemsInSection(in: collectionView, numberOfItemsInSection: section, with: frcArtist)
        case collectionViewAlbum:
            return fetchedItemsInSection(in: collectionView, numberOfItemsInSection: section, with: frcAlbum)
        default:
            return fetchedItemsInSection(in: collectionView, numberOfItemsInSection: section, with: frcTrack)
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        switch collectionView {
        case collectionViewArtist:
            guard let frcArtist = frcArtist else { fatalError("Should never happen!") }
            let artist = frcArtist.object(at: indexPath)
            let artistCell: ArtistGridCell = collectionViewArtist.dequeueReusableCell(forIndexPath: indexPath)
            return artistCell.configured(with: artist)
        case collectionViewAlbum:
            guard let frcAlbum = frcAlbum else { fatalError("Should never happen!") }
            let album = frcAlbum.object(at: indexPath)
            let albumCell: AlbumGridCell = collectionViewAlbum.dequeueReusableCell(forIndexPath: indexPath)
            return albumCell.configured(with: album)
        default:
            guard let frcTrack = frcTrack else { fatalError("Should never happen!") }
            let track = frcTrack.object(at: indexPath)
            let trackCell: TrackGridCell = collectionViewTrack.dequeueReusableCell(forIndexPath: indexPath)
            return trackCell.configured(with: track)
        }
    }
}




// MARK:- CollectionView Layout
extension MainVC: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        switch collectionView {
        case collectionViewArtist:
            return collectionView.sizeForItemForHorizontalScrolling(indexPath, columns: 2, layout: collectionViewLayout)
        case collectionViewAlbum:
            return collectionView.sizeForItemForHorizontalScrolling(indexPath, columns: 1, layout: collectionViewLayout)
        default:
            return collectionView.sizeForItemForHorizontalScrolling(indexPath, columns: 1, layout: collectionViewLayout)
        }
    }
}


// MARK:- Action activateSearch(), func openSearch()
extension MainVC {
    @IBAction func activateSearch(_ sender: UIBarButtonItem) {
        openSearch()
    }
    
    func openSearch() {
        let vc = SearchController.initial()
        vc.dependency = dependency
        show(vc, sender: self)
        searchController = vc
    }
    
    func initiateAlbumSearch(artistID: String?) {
        guard let dataManager = dataManager, let id = artistID else { return }
        dataManager.searchAlbums(for: id, itemType: .albums) {
            count, dataError in
            print(count)
        }
    }
    
    func openArtist(index: Int) {
        let vc = ArtistVC.instantiate(fromStoryboardNamed: "Main")
        vc.dependency = dependency
        vc.artist = frcArtist!.fetchedObjects![index]
        show(vc, sender: self)
    }
    
    func openAlbum(index: Int) {
        let vc = AlbumVC.instantiate(fromStoryboardNamed: "Main")
        vc.dependency = dependency
        vc.album = frcAlbum!.fetchedObjects![index]
        show(vc, sender: self)
    }
    
    func openTrack(index: Int) {
        let vc = TrackVC.instantiate(fromStoryboardNamed: "Main")
        vc.dependency = dependency
        vc.track = frcTrack!.fetchedObjects![index]
        show(vc, sender: self)
    }
}










