//
//  AlbumVC.swift
//  Spotifier
//
//  Created by Nemanja Gagic on 6/30/17.
//  Copyright © 2017 Nemanja Gagic. All rights reserved.
//

import UIKit
import CoreData
import Kingfisher
import AVFoundation

class AlbumVC: UIViewController, StoryboardLoadable, NeedsDependency {
    
    var dependency: Dependency? {
        didSet {
            if !self.isViewLoaded { return }
            prepareTrackDataSource()
        }
    }
    
    fileprivate var dataManager: DataManager? { return dependency?.dataManager }
    fileprivate var moc: NSManagedObjectContext? { return dependency?.coreDataContext }
    
    fileprivate var frc: NSFetchedResultsController<Track>?
    
    var album: Album? {
        didSet {
            initiateTracksSearch(albumID: album?.albumId)
        }
    }
    
    fileprivate var audioPlayer = AVPlayer()
    
    fileprivate var currentIndex: Int = 0 {
        didSet {
            currentIndexPath = [currentIndex]
        }
    }
    
    fileprivate var currentIndexPath: IndexPath?
    
    fileprivate var selectedTrack: Track? {
        didSet {
            downloadAndPlaySongPreview()
            trackLabel.text = (self.selectedTrack?.previewLink) != nil ? "Now playing: \((self.selectedTrack?.name)!)" : "Song preview not available"
        }
    }
    
    fileprivate var selectedCell: TrackCell? { return collectionView.cellForItem(at: currentIndexPath!) as? TrackCell }
    
    
    @IBOutlet fileprivate weak var imageView: UIImageView!
    @IBOutlet fileprivate weak var albumLabel: UILabel!
    @IBOutlet fileprivate weak var artistLabel: UILabel!
    @IBOutlet fileprivate weak var trackLabel: UILabel!
    @IBOutlet fileprivate weak var collectionView: UICollectionView!
}

extension AlbumVC {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureAlbum()
        prepareTrackDataSource()
    }
    
}

extension AlbumVC {
    
    func configureAlbum() {
        guard let albumName = album?.name else { return }
        albumLabel.text = albumName
        
        guard let artistName = album?.artistNames else { return artistLabel.text = "Unknown artist" }
        artistLabel.text = artistName
        
        guard let url = album?.imageURL else { return imageView.image = UIImage(named: "EmptyImage") }
        imageView.kf.setImage(with: url)
    }
    
    func initiateTracksSearch(albumID: String?) {
        guard let dataManager = dataManager, let id = albumID else { return }
        
        dataManager.searchTracks(for: id, itemType: .tracks) {
            count, dataError in
            print(count)
        }
    }
    
    func initiateTrackInfo(trackId: String?) {
        guard let dataManager = dataManager, let id = trackId else { return }
        
        dataManager.searchTrackInfo(for: id, itemType: nil) {
            (count, dataError) in
            print(count)
        }
    }
    
    
    func downloadAndPlaySongPreview() {
        guard let link = selectedTrack?.previewLink, let url = URL(string: link) else { return }
        audioPlayer = AVPlayer(url: url)
        audioPlayer.volume = 1.0
        audioPlayer.play()
    }
    
    
    
    
    func prepareTrackDataSource() {
        
        initiateTracksSearch(albumID: album?.albumId)
        
        guard let moc = moc, let id = album?.albumId else {
            frc = nil
            collectionView.reloadData()
            return
        }
        
        let sort = NSSortDescriptor(key: Track.Attributes.trackNumber, ascending: true)
        let predicate = NSPredicate(format: "%K == %@", Track.Attributes.albumId, id)
        frc = Track.fetchedResultsController(in: moc,
                                             predicate: predicate,
                                             sortedWith: [sort])
        
        frc?.delegate = self
        
        if ((try? frc?.performFetch()) != nil) {
            collectionView.reloadData()
        }
    }
}


extension AlbumVC: NSFetchedResultsControllerDelegate {
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        collectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        currentIndex = indexPath.row
        selectedTrack = frc!.fetchedObjects![currentIndex]
//        selectedCell = collectionView.cellForItem(at: indexPath) as? TrackCell
        selectedCell?.highlightSelectedCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        currentIndex = indexPath.row
        selectedTrack = frc!.fetchedObjects![currentIndex]
//        selectedCell = collectionView.cellForItem(at: indexPath) as? TrackCell
        selectedCell?.resetDeselectedCell()
    }
}

extension AlbumVC: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return fetchedNumberOfSections(in: collectionView, with: frc)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return fetchedItemsInSection(in: collectionView, numberOfItemsInSection: section, with: frc)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let frc = frc else { fatalError("Should never happen!") }
        let track = frc.object(at: indexPath)
        let cell: TrackCell = collectionView.dequeueReusableCell(forIndexPath: indexPath)
        return cell.configured(with: track)
    }
}



extension AlbumVC: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return collectionView.sizeForItemForHorizontalScrolling(indexPath, columns: 2, layout: collectionViewLayout)
    }
}


extension AlbumVC {
    
    @IBAction func play(_ sender: Any) {
        if selectedTrack == nil {
            selectedTrack = frc!.fetchedObjects![currentIndex]
        }
        playSong()
    }
    
    @IBAction func pause(_ sender: Any) {
        pauseSong()
    }
    
    @IBAction func previous(_ sender: Any) {
        playPreviousSong()
    }
    
    @IBAction func next(_ sender: Any) {
        playNextSong()
    }
    
    
    func playSong() {
        currentIndexPath = [currentIndex]
//        selectedCell = collectionView.cellForItem(at: currentIndexPath!) as? TrackCell
        selectedCell?.highlightSelectedCell()
        audioPlayer.play()
    }
    
    func pauseSong() {
        audioPlayer.pause()
    }
    
    func playPreviousSong() {
        if currentIndex == 0 {
            currentIndex += 1
        }
        currentIndex -= 1
        selectedTrack = frc!.fetchedObjects![currentIndex]
        audioPlayer.play()
    }
    
    func playNextSong() {
        
        if currentIndex == (frc?.fetchedObjects!.count)! - 1 {
            currentIndex -= 1
        }
        currentIndex += 1
        selectedTrack = frc!.fetchedObjects![currentIndex]
        audioPlayer.play()
    }
    
}

extension UICollectionViewCell {
    
    func highlightSelectedCell() {
        layer.borderWidth = 3.0
        layer.borderColor = UIColor.red.cgColor
    }
    
    func resetDeselectedCell() {
        layer.borderWidth = 0.0
        layer.borderColor = nil
    }
}
