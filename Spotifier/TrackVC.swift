//
//  TrackVC.swift
//  Spotifier
//
//  Created by Nemanja Gagic on 7/25/17.
//  Copyright © 2017 Nemanja Gagic. All rights reserved.
//

import UIKit
import CoreData
import Kingfisher
import AVFoundation

class TrackVC: UIViewController, StoryboardLoadable, NeedsDependency {
    
    var dependency: Dependency? {
        didSet {
            if !self.isViewLoaded { return }
//            prepareTrackDataSource()
        }
    }
    
    fileprivate var dataManager: DataManager? { return dependency?.dataManager }
    fileprivate var moc: NSManagedObjectContext? { return dependency?.coreDataContext }
    
    
    var track: Track? {
        didSet {
            initiateTrackInfo(trackId: track?.trackId)
        }
    }
    
    fileprivate var audioPlayer = AVPlayer()
    
    @IBOutlet fileprivate weak var trackLabel: UILabel!
    @IBOutlet fileprivate weak var albumLabel: UILabel!
    @IBOutlet fileprivate weak var artistLabel: UILabel!
    @IBOutlet fileprivate weak var imageView: UIImageView!
    @IBOutlet fileprivate weak var albumImage: UIImageView!
    @IBOutlet fileprivate weak var collectionView: UICollectionView!
}

extension TrackVC {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureTrack()
        configureAlbumBox()
        downloadAndPlaySongPreview()
    }
}

extension TrackVC {
    
    func initiateTrackInfo(trackId: String?) {
        guard let dataManager = dataManager, let id = trackId else { return }
        
        dataManager.searchTrackInfo(for: id, itemType: nil) {
            (count, dataError) in
            print(count)
        }
    }
    
    func downloadAndPlaySongPreview() {
        guard let link = track?.previewLink, let url = URL(string: link) else { return  }
        audioPlayer = AVPlayer(url: url)
        audioPlayer.volume = 1.0
        audioPlayer.play()
    }
    
    func configureTrack() {
        
        imageView.image = UIImage(named: "song playing")
        
        guard let trackName = track?.name else { return }
        trackLabel.text = trackName
        
        guard let albumName = track?.album?.name else { return albumLabel.text = "Unknown album" }
        albumLabel.text = albumName
        
        guard let artistName = track?.artistNames else {return artistLabel.text = "Unknown artist" }
        artistLabel.text = artistName
        
//        guard let url = album?.imageURL else { return imageView.image = UIImage(named: "EmptyImage") }
//        imageView.kf.setImage(with: url)
    }
    
    
    func configureAlbumBox() {
        
        guard let albumUrl = track?.album?.imageURL else { return albumImage.image = UIImage(named: "EmptyImage") }
        albumImage.kf.setImage(with: albumUrl)
    }
}
