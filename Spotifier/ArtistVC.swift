//
//  ArtistVC.swift
//  Spotifier
//
//  Created by Nemanja Gagic on 6/29/17.
//  Copyright © 2017 Nemanja Gagic. All rights reserved.
//

import UIKit
import CoreData
import Kingfisher


class ArtistVC: UIViewController, StoryboardLoadable, NeedsDependency {
    
    var dependency: Dependency? {
        didSet {
            if !self.isViewLoaded { return }
            prepareAlbumDataSource()
        }
    }
    
    fileprivate var dataManager: DataManager? { return dependency?.dataManager }
    fileprivate var moc: NSManagedObjectContext? { return dependency?.coreDataContext }
    
    var artist: Artist? {
        didSet {
            initiateAlbumSearch(artistID: artist!.artistId)
        }
    }
    
    fileprivate var frc: NSFetchedResultsController<Album>?
    
    @IBOutlet fileprivate weak var imageView: UIImageView!
    @IBOutlet fileprivate weak var artistLabel: UILabel!
    @IBOutlet fileprivate weak var popularityLabel: UILabel!
    @IBOutlet fileprivate weak var followersLabel: UILabel!
    @IBOutlet fileprivate weak var collectionView: UICollectionView!
}


extension ArtistVC {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureArtist()
        prepareAlbumDataSource()
    }
}


extension ArtistVC {
    
    func initiateAlbumSearch(artistID: String?) {
        guard let dataManager = dataManager, let id = artistID else { return }
        dataManager.searchAlbums(for: id, itemType: .albums) {
            count, dataError in
            print(count)
        }
    }
    
    func configureArtist() {
        guard let artistName = artist?.name else { return }
        artistLabel.text = artistName
        guard let popularity = artist?.popularity else { return popularityLabel.text = "Unpopular" }
        popularityLabel.text = "Popularity: \(popularity)"
        guard let followersCount = artist?.followersCount else { return followersLabel.text = "No followers" }
        followersLabel.text = "Followers: \(followersCount )"
        guard let url = artist?.imageURL else { return imageView.image = UIImage(named: "EmptyImage") }
        imageView.kf.setImage(with: url)
    }
    
    func prepareAlbumDataSource() {
        
        initiateAlbumSearch(artistID: artist?.artistId)

        guard let moc = moc, let id = artist?.artistId else {
            frc = nil
            collectionView.reloadData()
            return
        }
        
        let sort = NSSortDescriptor(key: Album.Attributes.dateReleased, ascending: false)
        let predicate = NSPredicate(format: "%K == %@", Album.Attributes.artistId, id)
        
        frc = Album.fetchedResultsController(in: moc, predicate: predicate, sortedWith: [sort])
        frc?.delegate = self
        
        guard ((try? frc?.performFetch()) != nil) else { return }
        collectionView.reloadData()
//            if frc?.fetchedObjects?.count ?? 0 == 0 {
//            }
//        }
    }
}

extension ArtistVC: NSFetchedResultsControllerDelegate {
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        collectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = AlbumVC.instantiate(fromStoryboardNamed: "Main")
        vc.dependency = dependency
        vc.album = frc!.fetchedObjects![indexPath.row]
        show(vc, sender: self)
    }
}

extension ArtistVC: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return fetchedNumberOfSections(in: collectionView, with: frc)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return fetchedItemsInSection(in: collectionView, numberOfItemsInSection: section, with: frc)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let frc = frc else { fatalError("Should never happen!") }
        let album = frc.object(at: indexPath)
        let cell: AlbumCell = collectionView.dequeueReusableCell(forIndexPath: indexPath)
        return cell.configured(with: album)
    }
}

extension ArtistVC: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return collectionView.sizeForItemForHorizontalScrolling(indexPath, columns: 2, layout: collectionViewLayout)
    }
}







