//
//  SearchController.swift
//  Spotifier
//
//  Created by Nemanja Gagic on 6/12/17.
//  Copyright © 2017 Nemanja Gagic. All rights reserved.
//

import UIKit
import CoreData

class SearchController: UIViewController, StoryboardLoadable, NeedsDependency {
    
    // MARK: Dependency
    var dependency: Dependency? {
        didSet {
            if !self.isViewLoaded { return }
            prepareDataSource()
        }
    }
    
    fileprivate var dataManager: DataManager? { return dependency?.dataManager }
    fileprivate var moc: NSManagedObjectContext? { return dependency?.coreDataContext }
    
    // MARK:- FRC
    
    fileprivate var frcArtist: NSFetchedResultsController<Artist>?
    fileprivate var frcAlbum: NSFetchedResultsController<Album>?
    fileprivate var frcTrack: NSFetchedResultsController<Track>?
    
    fileprivate var searchTerm: String? {
        didSet {
            prepareDataSource()
            initiateArtistSearch()
            initiateAlbumSearch()
            initiateTrackSearch()
        }
    }
    
    
    @IBOutlet weak var searchView: UIVisualEffectView!
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        collectionView.contentInset.top = searchView.frame.origin.y + searchView.frame.size.height
    }
    
    
    
    // MARK:- CollectionView and segment control
    @IBOutlet fileprivate weak var collectionView: UICollectionView!
    
    @IBOutlet weak var searchType: UISegmentedControl!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    // MARK:- ViewDidLoad, register search cell
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.register(SearchCell.self)
        
        prepareDataSource()
        
        self.automaticallyAdjustsScrollViewInsets = false
        
    }
}

// MARK:- initiateSearch(), prepareDataSource()

fileprivate extension SearchController {
    
    func initiateArtistSearch() {
        guard let dataManager = dataManager, let searchTerm = searchTerm else { return }
        dataManager.search(for: searchTerm, type: .artist) {
            // [weak self]
            count, dataError in
            // if error OR count == 0, show alert
        }
    }
    
    func initiateAlbumSearch() {
        guard let dataManager = dataManager, let searchTerm = searchTerm else { return }
        dataManager.search(for: searchTerm, type: .album) {
            count, dataError in
        }
    }
    
    func initiateTrackSearch() {
        guard let dataManager = dataManager, let searchTerm = searchTerm else { return }
        dataManager.search(for: searchTerm, type: .track) {
            count, dataError in
        }
    }
            
    
    func prepareDataSource() {
        
        prepareArtistDataSource()
        prepareAlbumDataSource()
        prepareTrackDataSource()
    }
}

extension SearchController {
    
    fileprivate func prepareArtistDataSource() {
        guard let moc = moc, let searchTerm = searchTerm else {
            frcArtist = nil
            collectionView.reloadData()
            return
        }
        
        let sort = NSSortDescriptor(key: Artist.Attributes.name, ascending: true)
        let sort2 = NSSortDescriptor(key: Artist.Attributes.popularity, ascending: false)
        let predicate = NSPredicate(format: "%K contains[cd] %@", Artist.Attributes.name, searchTerm)
        
        frcArtist = Artist.fetchedResultsController(in: moc,
                                                    predicate: predicate,
                                                    sortedWith: [sort, sort2])
        
        frcArtist?.delegate = self
        
        if ((try? frcArtist?.performFetch()) != nil) {
            collectionView.reloadData()
        }
    }
    
    fileprivate func prepareAlbumDataSource() {
        guard let moc = moc, let searchTerm = searchTerm else {
            frcAlbum = nil
            collectionView.reloadData()
            return
        }
        
        let sort = NSSortDescriptor(key: Album.Attributes.name, ascending: true)
        let sort2 = NSSortDescriptor(key: Album.Attributes.dateReleased, ascending: false)
        let predicate = NSPredicate(format: "%K contains[cd] %@", Album.Attributes.name, searchTerm)
        
        frcAlbum = Album.fetchedResultsController(in: moc,
                                                  predicate: predicate,
                                                  sortedWith: [sort, sort2])
        
        frcAlbum?.delegate = self
        
        if ((try? frcAlbum?.performFetch()) != nil) {
            collectionView.reloadData()
        }
    }
    
    fileprivate func prepareTrackDataSource() {
        guard let moc = moc, let searchTerm = searchTerm else {
            frcTrack = nil
            collectionView.reloadData()
            return
        }
        
        let sort = NSSortDescriptor(key: Track.Attributes.name, ascending: true)
        let sort2 = NSSortDescriptor(key: Track.Attributes.popularity, ascending: false)
        let predicate = NSPredicate(format: "%K contains[cd] %@", Track.Attributes.name, searchTerm)
        
        frcTrack = Track.fetchedResultsController(in: moc,
                                                  predicate: predicate,
                                                  sortedWith: [sort, sort2])
        
        frcTrack?.delegate = self
        
        if ((try? frcTrack?.performFetch()) != nil) {
            collectionView.reloadData()
        }
    }
}




// MARK:- FRC Delegate
extension SearchController: NSFetchedResultsControllerDelegate {
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        collectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        switch searchType.selectedSegmentIndex {
        case 0:
            openArtist(index: indexPath.row)
        case 1:
            openAlbum(index: indexPath.row)
        default:
            openTrack(index: indexPath.row)
        }
    }
}

// MARK:- UICollectionView DataSource
extension SearchController: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        switch searchType.selectedSegmentIndex {
        case 0:
            return fetchedNumberOfSections(in: collectionView, with: frcArtist)
        case 1:
            return fetchedNumberOfSections(in: collectionView, with: frcAlbum)
        default:
            return fetchedNumberOfSections(in: collectionView, with: frcTrack)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        switch searchType.selectedSegmentIndex {
        case 0:
            return fetchedItemsInSection(in: collectionView, numberOfItemsInSection: section, with: frcArtist)
        case 1:
            return fetchedItemsInSection(in: collectionView, numberOfItemsInSection: section, with: frcAlbum)
        default:
            return fetchedItemsInSection(in: collectionView, numberOfItemsInSection: section, with: frcTrack)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        switch searchType.selectedSegmentIndex {
        case 0:
            guard let frcArtist = frcArtist else { fatalError("Should never happen!") }
            let cell: SearchCell = collectionView.dequeueReusableCell(forIndexPath: indexPath)
            let artist = frcArtist.object(at: indexPath)
            return cell.configuredWithArtist(with: artist)
        case 1:
            guard let frcAlbum = frcAlbum else { fatalError("Should never happen!") }
            let cell: SearchCell = collectionView.dequeueReusableCell(forIndexPath: indexPath)
            let album = frcAlbum.object(at: indexPath)
            return cell.configuredWithAlbum(with: album)
        default:
            guard let frcTrack = frcTrack else { fatalError("Should never happen!") }
            let cell: SearchCell = collectionView.dequeueReusableCell(forIndexPath: indexPath)
            let track = frcTrack.object(at: indexPath)
            return cell.configuredWithTrack(with: track)
        }
    }
}

// MARK:- CollectionView Layout
extension SearchController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        
//        collectionView.sizeForItemFromUI(collectionView, indexPath, layout: collectionViewLayout)
        let bounds = collectionView.bounds
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout

        var size = flowLayout.itemSize
        size.width = bounds.width
        return size
    }
}

// MARK:- Search Bar text editing
extension SearchController {
    @IBAction func didChangeTextfield(_ sender: UITextField) {
        guard let str = sender.text else {
            searchTerm = nil
            return
        }
        
        if str.characters.count == 0 {
            searchTerm = nil
            return
        }
        searchTerm = str
    }
}


fileprivate extension SearchController {
    
    func openArtist(index: Int) {
        let vc = ArtistVC.instantiate(fromStoryboardNamed: "Main")
        vc.dependency = dependency
        vc.artist = frcArtist!.fetchedObjects![index]
        show(vc, sender: self)
    }
    
    func openAlbum(index: Int) {
        let vc = AlbumVC.instantiate(fromStoryboardNamed: "Main")
        vc.dependency = dependency
        vc.album = frcAlbum!.fetchedObjects![index]
        show(vc, sender: self)
    }
    
    func openTrack(index: Int) {
        let vc = TrackVC.instantiate(fromStoryboardNamed: "Main")
        vc.dependency = dependency
        vc.track = frcTrack!.fetchedObjects![index]
        show(vc, sender: self)
    }
}
















