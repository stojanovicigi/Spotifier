//
//  AppDelegate.swift
//  Spotifier
//
//  Created by Nemanja Gagic on 5/30/17.
//  Copyright © 2017 Nemanja Gagic. All rights reserved.
//

import UIKit
import RTCoreDataStack

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    var coreDataStack: RTCoreDataStack?
    
    var spotifyManager: Spotify?
    var dataManager: DataManager?
    
    
    func application(_ application: UIApplication, willFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey : Any]? = nil) -> Bool {
        
        spotifyManager = Spotify.shared
        
        coreDataStack = RTCoreDataStack {
            // CDS is ready
            [unowned self] in
            
            self.configureManagers()
            
            let dependency = Dependency(coreDataContext: self.coreDataStack!.mainContext,
                                        dataManager: self.dataManager)
            
            if let nc = self.window?.rootViewController as? UINavigationController {
                
                if let vc = nc.topViewController as? MainVC {
                    vc.dependency = dependency
                }
                if let vc = nc.topViewController as? ArtistVC {
                    vc.dependency = dependency
                }
                if let vc = nc.topViewController as? AlbumVC {
                    vc.dependency = dependency
                }
                if let vc = nc.topViewController as? TrackVC {
                    vc.dependency = dependency
                }
            }
            
            self.dataManager?.search(for: "taylor", type: .artist, callback: {
                count, _ in
                print(count)
            })
            self.dataManager?.search(for: "summer", type: .album, callback: {
                count, _ in
                print(count)
            })
            self.dataManager?.search(for: "good", type: .track, callback: {
                count, _ in
                print(count)
            })
        }
        return true
    }
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        return true
    }
    
}

extension AppDelegate {
    func configureManagers() {
        
        guard let cds = coreDataStack, let sm = spotifyManager else {
            fatalError()
        }
        
        dataManager = DataManager(spotifyManager: sm, coreDataStack: cds)
    }
}





