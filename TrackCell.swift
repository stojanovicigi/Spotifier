//
//  TrackCell.swift
//  Spotifier
//
//  Created by Nemanja Gagic on 7/12/17.
//  Copyright © 2017 Nemanja Gagic. All rights reserved.
//

import UIKit
import Kingfisher

class TrackCell: UICollectionViewCell, NibReusableView {
    
    
    @IBOutlet weak var photoView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        cleanup()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        cleanup()
    }
    
    func cleanup() {
        nameLabel.text = nil
        photoView.image = nil
    }
    
    
    func configured(with track: Track) -> TrackCell {
        self.nameLabel.text = track.name
        self.photoView.image = UIImage(named: "play song 2")
        return self
    }
}
