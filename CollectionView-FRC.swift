//
//  CollectionView-FRC.swift
//  Spotifier
//
//  Created by Nemanja Gagic on 8/9/17.
//  Copyright © 2017 Nemanja Gagic. All rights reserved.
//

import UIKit
import CoreData

extension UICollectionViewCell {
    static var cell: UICollectionViewCell { return self.init() }
    static var object: Any { return self.init()}
}

extension UICollectionViewDataSource {
    
    func fetchedNumberOfSections<T>(in collectionView: UICollectionView, with fetchedResultsController: NSFetchedResultsController<T>?) -> Int {
        guard let frc = fetchedResultsController else { return 0 }
        guard let sections = frc.sections else { return 0 }
        return sections.count
    }
    
    func fetchedItemsInSection<T>(in collectionView: UICollectionView, numberOfItemsInSection section: Int, with fetchedResultsController: NSFetchedResultsController<T>?) -> Int {
        guard let frc = fetchedResultsController else { return 0 }
        guard let sections = frc.sections else { return 0 }
        return sections[section].numberOfObjects
    }
    
    func fetchedCellForItemAtIndexPath<T, C: UICollectionViewCell>(_ collectionView: UICollectionView,
                                                                   at indexPath: IndexPath,
                                                                   cellType: C,
                                                                   with fetchedResultsController: NSFetchedResultsController<T>?) -> UICollectionViewCell {
        
        guard let frc = fetchedResultsController else { fatalError("Should never happen!") }
        var object: T = UICollectionViewCell.object as! T
        object = frc.object(at: indexPath)
        var cell: C = UICollectionViewCell.cell as! C
        cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: self), for: indexPath) as! C
        return cell
    }
    
}


extension NSFetchedResultsControllerDelegate{
    
//    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        let vc = ArtistVC.instantiate(fromStoryboardNamed: "Main")
//        vc.dependency = dependency
//        vc.artist = frcArtist!.fetchedObjects![index]
//        show(vc, sender: self)
//    }
    
    func openSelectedItem<T, V: UIViewController>(_ collectionView: UICollectionView,
                                                  at indexPath: IndexPath,
                                                  show viewController: V,
                                                  fetchedObject: inout T,
                                                  with fetchedResultsController: NSFetchedResultsController<T>?) where V: StoryboardLoadable {
        //        let vc = ArtistVC.instantiate(fromStoryboardNamed: "Main")
        //        vc.dependency = dependency
        //        vc.artist = frcArtist!.fetchedObjects![index]
        //        show(vc, sender: self)
    }
    
}












