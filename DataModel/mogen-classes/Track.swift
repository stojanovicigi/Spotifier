import Foundation
import CoreData
import Marshal
import RTCoreDataStack

@objc(Track)
public class Track: NSManagedObject {

    // MARK: - Life cycle methods

    public override init(entity: NSEntityDescription, insertInto context: NSManagedObjectContext?) {
        super.init(entity: entity, insertInto: context)
    }

	required public init?(managedObjectContext moc: NSManagedObjectContext) {
		guard let entity = NSEntityDescription.entity(forEntityName: Track.entityName, in: moc) else { return nil }
		super.init(entity: entity, insertInto: moc)
	}
}

extension Track {
    var imageURL: URL? {
        guard let imageLink = imageLink else { return nil }
        return URL(string: imageLink)
    }
    var artistImageURL: URL? {
        guard let artistImage = artistImage else { return nil }
        return URL(string: artistImage)
    }
    
    
}


// MARK:- ManagedObjectsType
// replaces FRC, predicate, sortDescriptors
extension Track: ManagedObjectType {}

extension Track: UnmarshalingWithContext {
    
    public static func value(from object: MarshaledObject, inContext context: NSManagedObjectContext) throws -> Track {
        guard let mo = Track(managedObjectContext: context) else {
            throw DataError.coreDataCreateFailed
        }
        
        do {
            mo.trackId = try object.value(for: "id")
            mo.name = try object.value(for: "name")
            mo.spotifyURI = try? object.value(for: "uri")
            
            if let arrArtists: [Artist] = try object.value(for: "artists", inContext: context) {
                
//                mo.artistImage = arrArtists.first?.imageLink
                                
                var artists: Set<Artist> = []
                var artistNames: [String] = []
                var artistIds: [String] = []
                var artistImages: [String] = []
                
                for a in arrArtists {
                    artists.insert(a)
                    artistNames.append(a.name)
                    artistIds.append(a.artistId)
                    
                    if let imageLink = a.imageLink {
                        artistImages.append(imageLink)
                    }
                }
                
                mo.artists = artists
                mo.artistNames = artistNames.joined(separator: " & ")
                mo.artistIds = artistIds.joined(separator: " & ")
                mo.artistImage = artistImages.joined(separator: ", ")
            }
            
            if let album: Album = try object.value(for: "album", inContext: context) {
                mo.album = album
                mo.albumId = album.albumId
            }
            
            
            if let value: String = try? object.value(for: "preview_url") {
                mo.previewLink = value
            }
            
            if let value: Bool = try? object.value(for: "explicit") {
                mo.isExplicit = value
            }
            
            if let value: Int64 = try? object.value(for: "duration_ms") {
                mo.durationMilliseconds = value
            }
            
            if let value: Int64 = try? object.value(for: "popularity") {
                mo.popularity = value
            }
            
            if let value: Int64 = try? object.value(for: "disc_number") {
                mo.discNumber = value
            }
            
            if let arr: [String] = try? object.value(for: "available_markets") {
                mo.csvAvailableMarkets = arr.joined(separator: ",")
            }
            
            return mo
            
            
        } catch let jsonError {
            context.delete(mo)
            throw DataError.jsonError(jsonError as! MarshalError)
        }
    }
}
