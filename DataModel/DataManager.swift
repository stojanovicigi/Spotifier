//
//  DataManager.swift
//  Spotifier
//
//  Created by Nemanja Gagic on 6/5/17.
//  Copyright © 2017 Nemanja Gagic. All rights reserved.
//

// MARK: Frameworks
import Foundation
import RTCoreDataStack
import Marshal
import CoreData

// MARK:- Data error types
enum DataError: Error {
    case apiError(SpotifyError)
    case invalidJSON
    case coreDataCreateFailed
    case coreDataSaveFailed
    case jsonError(MarshalError)
}

// MARK:- Data model
final class DataManager {
    
    let spotifyManager: Spotify
    let coreDataStack: RTCoreDataStack
    
    init(spotifyManager: Spotify, coreDataStack: RTCoreDataStack) {
        self.spotifyManager = spotifyManager
        self.coreDataStack = coreDataStack
    }
}


// MARK:- API wrappers, func search()
extension DataManager {
    
    typealias Callback = (Int, DataError?) -> Void
    
    // MARK: Post Nofifications
    // Make API call to preload data, post notification to inform rest of app
    func preload() {
        
        // Recommended way of notification
        let name = Notify.dataPreloadCompleted.name
        // SDK swift way of notification:
        // let name = Notification.Name(Notification.Name.DataManagerDataPreloadCompleted)
        let userInfo: [String: Any]? = nil
        let notification = Notification(name: name, object: self, userInfo: userInfo)
        NotificationCenter.default.post(notification)
    }
    
    
    func search(for term: String, type: Spotify.SearchType, callback: @escaping Callback) {
        
        // MARK: Path definition
        let path = Spotify.Path.search(term: term, type: type)
        
        // MARK: Implementation of call()
        spotifyManager.call(path: path) {
            [unowned self] json, error in
            
            // MARK: Error handling
            if let error = error {
                callback(0, DataError.apiError(error))
                return
            }
            
            guard let json = json else {
                callback(0, DataError.invalidJSON)
                return
            }
            
            // MARK: CoreData notification handling
            let moc = self.coreDataStack.importerContext()
            var count: Int = 0
            
            // MARK: CoreData update results
            switch type {
                
            case .artist:
                guard let result = json["artists"] as? JSON else { return }
                // guard let items = result["items"] as? [JSON] else { return }
                do {
                    var artists: [Artist] = try result.value(for: "items", inContext: moc)
                    let artistIDs: [String] = artists.flatMap({$0.artistId})
                    
                    let predicate = NSPredicate(format: "%K IN %@", Artist.Attributes.artistId, artistIDs)
                    
                    let existingObjects = Artist.fetch(in: moc, includePending: false, predicate: predicate)
                    
                    let existingIDs = existingObjects.flatMap({$0.artistId})
                    
                    // Check already existing newly created artists
                    var updatedArtists = artists.filter({ existingIDs.contains($0.artistId) })
                    
                    // Leave only fully new artists
                    artists = artists.filter({ !updatedArtists.contains($0) })
                    
                    // Update exising objects
                    for io in updatedArtists {
                        guard let eo = existingObjects.filter({ $0.artistId == io.artistId }).first else { continue }
                        // update each property, from io to eo
                        eo.name = io.name
                        eo.artistId = io.artistId
                        eo.albums = io.albums
                        eo.csvGenres = io.csvGenres
                        eo.followersCount = io.followersCount
                        eo.imageLink = io.imageLink
                        eo.popularity = io.popularity
                        eo.spotifyURI = io.spotifyURI
                    }
                    
                    // MARK: Delete (double) existing records
                    
                    for mo in updatedArtists {
                        moc.delete(mo)
                    }
                    updatedArtists.removeAll()
                    
                    // MARK: Records count
                    count = artists.count + existingObjects.count
                    
                } catch let jsonError {
                    callback(0, DataError.jsonError(jsonError as! MarshalError))
                    return
                }
                
            case .album:
                guard let result = json["albums"] as? JSON else { return }
                
                do {
                    var albums: [Album] = try result.value(for: "items", inContext: moc)
                    let albumIDs: [String] = albums.flatMap({$0.albumId})
                    
                    let predicate = NSPredicate(format: "%K IN %@", Album.Attributes.albumId, albumIDs)
                    
                    let existingObjects = Album.fetch(in: moc, includePending: false, predicate: predicate)
                    
                    let existingIDs = existingObjects.flatMap({$0.albumId})
                    
                    var updatedAlbums = albums.filter({ existingIDs.contains($0.albumId) })
                    
                    albums = albums.filter({ !updatedAlbums.contains($0) })
                    
                    for io in updatedAlbums {
                        guard let eo = existingObjects.filter({ $0.albumId == io.albumId }).first else { continue }
                        eo.name = io.name
                        eo.artistNames = io.artistNames
                        eo.artistIds = io.artistIds
                        eo.artists = io.artists
                        eo.csvAvailableMarkets = io.csvAvailableMarkets
                        eo.dateReleased = io.dateReleased
                        eo.dateReleasedPrecision = io.dateReleasedPrecision
                        eo.imageLink = io.imageLink
                        eo.labelName = io.labelName
                    }
                    
                    for mo in updatedAlbums {
                        moc.delete(mo)
                    }
                    updatedAlbums.removeAll()
                    
                    count = albums.count + existingObjects.count
                    
                } catch let jsonError {
                    callback(0, DataError.jsonError(jsonError as! MarshalError))
                    return
                }
                
                
            case .track:
                guard let result = json["tracks"] as? JSON else { return }
                
                do {
                    var tracks: [Track] = try result.value(for: "items", inContext: moc)
                    let trackIDs: [String] = tracks.flatMap({$0.trackId})
                    
                    let predicate = NSPredicate(format: "%K IN %@", Track.Attributes.trackId, trackIDs)
                    
                    let existingObjects = Track.fetch(in: moc, includePending: false, predicate: predicate)
                    
                    let existingIDs = existingObjects.flatMap({$0.trackId})
                    
                    var updatedTracks = tracks.filter({ existingIDs.contains($0.trackId) })
                    
                    tracks = tracks.filter({ !updatedTracks.contains($0) })
                    
                    for io in updatedTracks {
                        guard let eo = existingObjects.filter({ $0.trackId == io.trackId }).first else { continue }
                        eo.name = io.name
                        eo.albumId = io.albumId
                        eo.artistNames = io.artistNames
                        eo.artistImage = io.artistImage
                        eo.artistIds = io.artistIds
                        eo.imageLink = io.imageLink
                        eo.previewLink = io.previewLink
                        eo.popularity = io.popularity
                        eo.discNumber = io.discNumber
                        eo.durationMilliseconds = io.durationMilliseconds
                        eo.csvAvailableMarkets = io.csvAvailableMarkets
                        eo.isExplicit = io.isExplicit
                    }
                    
                    for mo in updatedTracks {
                        moc.delete(mo)
                    }
                    updatedTracks.removeAll()
                    
                    count = tracks.count + existingObjects.count
                    
                } catch let jsonError {
                    callback(0, DataError.jsonError(jsonError as! MarshalError))
                    return
                }
                
            case .playlist:
                break
            }
            
            // MARK: Save context
            do {
                if moc.hasChanges {
                    try moc.save()
                }
                callback(count, nil)
                
            } catch let error {
                print("Context Save failed due to: \(error)")
                callback(0, DataError.coreDataSaveFailed)
            }
        }
    }
    
    
    func searchAlbums(for id: String, itemType: Spotify.ItemType, callback: @escaping Callback) {
        
        let path = Spotify.Path.artists((id: id, itemType: itemType))
        
        spotifyManager.call(path: path) {
            [unowned self] json, error in
            
            if let error = error {
                callback(0, DataError.apiError(error))
                return
            }
            
            guard let json = json else {
                callback(0, DataError.invalidJSON)
                return
            }
            
            let moc = self.coreDataStack.importerContext()
            var count: Int = 0
            
            
            switch itemType {
                
            case .albums:
                //                guard let result = json as? JSON else { return }
                
                do {
                    var albums: [Album] = try json.value(for: "items", inContext: moc)
                    let albumIDs: [String] = albums.flatMap({$0.albumId})
                    
                    let predicate = NSPredicate(format: "%K IN %@", Album.Attributes.albumId, albumIDs)
                    
                    let existingObjects = Album.fetch(in: moc, includePending: false, predicate: predicate)
                    
                    let existingIDs = existingObjects.flatMap({$0.albumId})
                    
                    var updatedAlbums = albums.filter({ existingIDs.contains($0.albumId) })
                    
                    albums = albums.filter({ !updatedAlbums.contains($0) })
                    
                    for io in updatedAlbums {
                        guard let eo = existingObjects.filter({ $0.albumId == io.albumId }).first else { continue }
                        eo.name = io.name
                        eo.artistNames = io.artistNames
                        eo.artistId = id
                        eo.artistIds = io.artistIds
                        eo.artists = io.artists
                        eo.csvAvailableMarkets = io.csvAvailableMarkets
                        eo.dateReleased = io.dateReleased
                        eo.dateReleasedPrecision = io.dateReleasedPrecision
                        eo.imageLink = io.imageLink
                        eo.labelName = io.labelName
                    }
                    
                    
                    for mo in updatedAlbums {
                        moc.delete(mo)
                    }
                    updatedAlbums.removeAll()
                    
                    count = albums.count + existingObjects.count
                    
                } catch let jsonError {
                    callback(0, DataError.jsonError(jsonError as! MarshalError))
                    return
                }
                
            default:
                break
            }
            
            do {
                if moc.hasChanges {
                    try moc.save()
                }
                callback(count, nil)
                
            } catch let error {
                print("Context Save failed due to: \(error)")
                callback(0, DataError.coreDataSaveFailed)
            }
        }
    }
    
    
    
    func searchTracks(for id: String, itemType: Spotify.ItemType, callback: @escaping Callback) {
        
        let path = Spotify.Path.albums((id: id, itemType: itemType))
        
        spotifyManager.call(path: path) {
            [unowned self] json, error in
            
            if let error = error {
                callback(0, DataError.apiError(error))
                return
            }
            
            guard let json = json else {
                callback(0, DataError.invalidJSON)
                return
            }
            
            let moc = self.coreDataStack.importerContext()
            var count: Int = 0
            
            
            switch itemType {
                
            case .tracks:
                
                do {
                    var tracks: [Track] = try json.value(for: "items", inContext: moc)
                    let trackIDs: [String] = tracks.flatMap({$0.trackId})
                    
                    let predicate = NSPredicate(format: "%K IN %@", Track.Attributes.trackId, trackIDs)
                    
                    let existingObjects = Track.fetch(in: moc, includePending: false, predicate: predicate)
                    
                    let existingIDs = existingObjects.flatMap({$0.trackId})
                    
                    var updatedTracks = tracks.filter({ existingIDs.contains($0.trackId) })
                    
                    tracks = tracks.filter({ !updatedTracks.contains($0) })
                    
                    for io in updatedTracks {
                        guard let eo = existingObjects.filter({ $0.trackId == io.trackId }).first else { continue }
                        eo.name = io.name
                        eo.album = io.album
                        eo.albumId = id
                        eo.artistNames = io.artistNames
                        eo.artistImage = io.artistImage
                        eo.artistIds = io.artistIds
                        eo.imageLink = io.imageLink
                        eo.previewLink = io.previewLink
                        eo.popularity = io.popularity
                        eo.discNumber = io.discNumber
                        eo.durationMilliseconds = io.durationMilliseconds
                        eo.csvAvailableMarkets = io.csvAvailableMarkets
                        eo.isExplicit = io.isExplicit
                    }
                    
                    for mo in updatedTracks {
                        moc.delete(mo)
                    }
                    updatedTracks.removeAll()
                    
                    count = tracks.count + existingObjects.count
                    
                } catch let jsonError {
                    callback(0, DataError.jsonError(jsonError as! MarshalError))
                    return
                }
                
            default:
                break
            }
            
            do {
                if moc.hasChanges {
                    try moc.save()
                }
                callback(count, nil)
                
            } catch let error {
                print("Context Save failed due to: \(error)")
                callback(0, DataError.coreDataSaveFailed)
            }
        }
    }
    
    
    func searchTrackInfo(for id: String, itemType: Spotify.ItemType?, callback: @escaping Callback) {
        
        let path = Spotify.Path.tracks((id: id, itemType: itemType))
        
        spotifyManager.call(path: path) {
            [unowned self] json, error in
            
            if let error = error {
                callback(0, DataError.apiError(error))
                return
            }
            
            guard let json = json else {
                callback(0, DataError.invalidJSON)
                return
            }
            
            let moc = self.coreDataStack.importerContext()
            var count: Int = 0
            
            
            switch itemType {
                
            default:
                
                do {
                    var tracks: [Track] = try json.value(for: "items", inContext: moc)
                    let trackIDs: [String] = tracks.flatMap({$0.trackId})
                    
                    let predicate = NSPredicate(format: "%K IN %@", Track.Attributes.trackId, trackIDs)
                    
                    let existingObjects = Track.fetch(in: moc, includePending: false, predicate: predicate)
                    
                    let existingIDs = existingObjects.flatMap({$0.trackId})
                    
                    var updatedTracks = tracks.filter({ existingIDs.contains($0.trackId) })
                    
                    tracks = tracks.filter({ !updatedTracks.contains($0) })
                    
                    for io in updatedTracks {
                        guard let eo = existingObjects.filter({ $0.trackId == io.trackId }).first else { continue }
                        eo.name = io.name
                        eo.album = io.album
                        eo.albumId = id
                        eo.artistNames = io.artistNames
                        eo.artistImage = io.artistImage
                        eo.artistIds = io.artistIds
                        eo.imageLink = io.imageLink
                        eo.previewLink = io.previewLink
                        eo.popularity = io.popularity
                        eo.discNumber = io.discNumber
                        eo.durationMilliseconds = io.durationMilliseconds
                        eo.csvAvailableMarkets = io.csvAvailableMarkets
                        eo.isExplicit = io.isExplicit
                    }
                    
                    for mo in updatedTracks {
                        moc.delete(mo)
                    }
                    updatedTracks.removeAll()
                    
                    count = tracks.count + existingObjects.count
                    
                } catch let jsonError {
                    callback(0, DataError.jsonError(jsonError as! MarshalError))
                    return
                }
                
                
                do {
                    if moc.hasChanges {
                        try moc.save()
                    }
                    callback(count, nil)
                    
                } catch let error {
                    print("Context Save failed due to: \(error)")
                    callback(0, DataError.coreDataSaveFailed)
                }
            }
        }
    }
}









