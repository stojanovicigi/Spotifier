//
//  FetchedData.swift
//  Spotifier
//
//  Created by Nemanja Gagic on 8/18/17.
//  Copyright © 2017 Nemanja Gagic. All rights reserved.
//

import UIKit
import CoreData

final class DataSource: NeedsDependency {
    
    var dependency: Dependency?
    var dataManager: DataManager? { return dependency?.dataManager }
    var moc: NSManagedObjectContext? { return dependency?.coreDataContext }
    
    var frcArtist: NSFetchedResultsController<Artist>?
    var frcAlbum: NSFetchedResultsController<Album>?
    var frcTrack: NSFetchedResultsController<Track>?
    
}

extension DataSource {
    
    func prepareMainVCArtistDataSource() -> NSFetchedResultsController<Artist>? {
        guard let moc = moc else { return nil }
        
        let fr: NSFetchRequest<Artist> = Artist.fetchRequest()
        
        let sort = NSSortDescriptor(key: Artist.Attributes.popularity, ascending: false)
        let sort2 = NSSortDescriptor(key: Artist.Attributes.followersCount, ascending: false)
        fr.sortDescriptors = [sort, sort2]
        
        frcArtist = NSFetchedResultsController(fetchRequest: fr,
                                               managedObjectContext: moc,
                                               sectionNameKeyPath: nil,
                                               cacheName: nil)
        
        return frcArtist
    }
    
    
    func prepareMainVCAlbumDataSource() {
        guard let moc = moc else { return }
        
        let fr: NSFetchRequest<Album> = Album.fetchRequest()
        
        let sort = NSSortDescriptor(key: Album.Attributes.dateReleased, ascending: false)
        let sort2 = NSSortDescriptor(key: Album.Attributes.albumId, ascending: false)
        fr.sortDescriptors = [sort, sort2]
        
        frcAlbum = NSFetchedResultsController(fetchRequest: fr,
                                              managedObjectContext: moc,
                                              sectionNameKeyPath: nil,
                                              cacheName: nil)
    }
    
    func prepareMainVCTrackDataSource() {
        guard let moc = moc else { return }
        
        let fr: NSFetchRequest<Track> = Track.fetchRequest()
        
        let sort = NSSortDescriptor(key: Track.Attributes.popularity, ascending: false)
        fr.sortDescriptors = [sort]
        
        frcTrack = NSFetchedResultsController(fetchRequest: fr,
                                              managedObjectContext: moc,
                                              sectionNameKeyPath: nil,
                                              cacheName: nil)
    }
}

