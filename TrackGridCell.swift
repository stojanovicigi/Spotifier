//
//  TrackGridCell.swift
//  Spotifier
//
//  Created by Nemanja Gagic on 7/19/17.
//  Copyright © 2017 Nemanja Gagic. All rights reserved.
//

import Foundation
import Kingfisher

final class TrackGridCell: UICollectionViewCell, NibReusableView {
    
    @IBOutlet weak var photoView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var artistLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        cleanup()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        cleanup()
    }
    
    fileprivate func cleanup() {
        photoView.image = nil
        nameLabel.text = nil
        artistLabel.text = nil
    }
    
    
    func configured(with track: Track) -> TrackGridCell {
        self.nameLabel.text = track.name
        self.artistLabel.text = track.artistNames
        self.photoView.image = UIImage(named: "play song 2")
        return self
    }
    
    
}
























