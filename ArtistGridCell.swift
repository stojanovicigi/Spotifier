//
//  ArtistGridCell.swift
//  Spotifier
//
//  Created by Nemanja Gagic on 6/14/17.
//  Copyright © 2017 Nemanja Gagic. All rights reserved.
//

import UIKit
import Kingfisher

final class ArtistGridCell: UICollectionViewCell, NibReusableView {
    
    @IBOutlet weak var photoView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var followersLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        cleanup()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        cleanup()
    }
    
    func cleanup() {
        photoView.image = nil
        nameLabel.text = nil
        followersLabel.text = nil
    }
    
    
    func configured(with artist: Artist) -> ArtistGridCell {
        self.nameLabel.text = artist.name
        self.followersLabel.text = "Followers: \(artist.followersCount)"
        if let url = artist.imageURL {
            self.photoView.kf.setImage(with: url)
        } else {
            self.photoView.image = UIImage(named: "EmptyImage")
        }
        return self
    }
    
    
}























